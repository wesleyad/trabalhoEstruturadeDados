#include <iostream>

using namespace std;
const int tamanho = 10;

int i,maior,vetor,valor;

struct pontos {
    int x;
    int y;
};

struct pontos ponto[tamanho];

void incrementarPonto(int posicao){
    for(i=0; i < posicao; i++){
    cout << "Insira em X na posicao  "<<(i+1)<< " : ";
    cin >> ponto[i].x;
    }
    cout<<" "<< endl;
    for(i=0; i < posicao; i++){
    cout << "Insira em Y na posicao "<<(i+1)<< " : ";
    cin >> ponto[i].y;
    }
}

void maiorValorX(pontos ponto[], int posicao){
    for(i=0; i<posicao; i++){
        if (ponto[i].x > maior){
            maior = ponto[i].x;
        }
    }
   cout << "Maior valor de X: " <<maior<< endl;
}

void menorValorY(pontos ponto[], int posicao){
    int menor;
   for(i=0; i<posicao; i++){
        if (ponto[i].y < menor){
            menor = ponto[i].y;
        }
    }
   cout << "Menor valor de Y: " <<menor<< endl;
}

void encontraValorXeY(pontos ponto[], int posicao){
    cout << "Valor a encontrar em X e Y : ";
    cin >> valor;
    cout<<" "<<endl;
   for(i=0;i<posicao;i++){
        if(valor==ponto[i].x){
           cout << "Posicoes que contem o valor em X:" <<(i+1)<<endl;
        }
    }
    for(i=0;i<posicao;i++){
        if(valor==ponto[i].y){
            cout << "Posicoes que contem o valor em Y:" <<(i+1)<<endl;
        }
    }
}
int main()
{
    incrementarPonto(tamanho);
    cout<< " "<<endl;
    maiorValorX(ponto,tamanho);
    menorValorY(ponto,tamanho);
    cout<< " "<<endl;
    encontraValorXeY(ponto,tamanho);
   }
