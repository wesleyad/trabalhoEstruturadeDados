#include <iostream>

using namespace std;
const int tamanho=5;
struct registro{
    int codigo;
    char nome[60];
    char endereco[60];
};
registro *registros[tamanho];
void inicializar(registro *registros[],int tamanho){
    for (int i=0;i<tamanho;i++){
        registros[i]=new registro;
    }
}
void lerRegistros(registro *registros[],int tamanho){
    cout<< "Digite os campos dos registro"<<endl;
    for (int i=0;i <tamanho;i++){
        cout<< "nome: ";
        cin.getline(registros[i]->nome,60);
        cout<< "endereco: ";
        cin.getline(registros[i]->endereco,60);
        cout<< "codigo: ";
        cin>> registros[i]->codigo;
        cout<<endl;
        cin.ignore();
    }
}

void buscarSequencialmente(registro *registros[],int tamanho,int codigoAlvo){
     for (int i=0;i <tamanho;i++){
        if (registros[i]->codigo== codigoAlvo){
            cout<<"Registro encontrado"<<endl;
            cout<<"nome: "<<registros[i]->nome<<endl;
            cout<<"endereco: "<<registros[i]->endereco<<endl;
            cout<<"codigo: "<<registros[i]->codigo<<endl;
            cout<<endl;
            break;
        }
    }
}
int main(){
    inicializar(registros,tamanho);
    lerRegistros(registros,tamanho);
    int codigo;
    cout<< "Digite o codigo de busca: ";
    cin>> codigo;
    buscarSequencialmente(registros,tamanho,codigo);

    return 0;
}
