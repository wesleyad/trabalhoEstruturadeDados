#include <iostream>

using namespace std;

int **vetor;//ponteiro de ponteiro
int tamanho;

int** inicializar(int tamanho){
    int** vet = new int *[tamanho];
    for (int i=0; i <tamanho;i++){
        vet[i]=new int;
    }
    return vet;
}
void lerVetor(int *vetor[],int tamanho){
    for (int i=0; i<tamanho;i ++){
        if(vetor[i]!=NULL){
            cout<< "posicao "<<(i+1)<<":";
            cin >> *(vetor[i]);
        }
    }
}
void imprimirVetor(int **vetor,int tamanho){
    cout<<endl<<endl<< "Valor do vetor: "<<endl;
    for (int i=0; i <tamanho;i++){
        if (vetor[i]!=NULL){
            cout<< "posicao["<<(i+1)<<"]:"<<*(vetor[i])<<endl;
        }else{
        cout<< "posicao["<<(i+1)<<"]:"<<"null"<<endl;
        }
    }
}
int main()
{
    cout<<"digite o tamanho do vetor: ";
    cin>> tamanho;

    vetor=inicializar(tamanho);
    lerVetor(vetor,tamanho);
    imprimirVetor(vetor,tamanho);
    delete vetor;

    return 0;
}
