#include <iostream>

using namespace std;
const int tamanho=10;
int vetor[tamanho];
int media;
int i;
void inicializarVetor(int vet[],int tamanho)
{
    cout << "Inicializando o vetor " << endl;
    for (int i=0; i < tamanho; i++)
    {
        vetor[i]=0;
    }
}
void lerVetor(int vet[],int tamanho)
{
    cout << "Leia os valores do vetor"<<endl;
    for (int i=0; i<tamanho; i++)
    {
        cout << "Posicao "<<(i+1)<<": ";
        cin>>(vetor[i]);
    }
}
void MenorValor(int vet[], int tam)
{
    int menor;
    for(int i = 0; i < tam; i++)
    {
        if(menor>vet[i])
        {
            menor= vet[i];
        }
    }
    cout<<"O menor valor na matriz e: " <<menor;
}

void maiorValor(int vet[], int tam)
{
    int maior;
    for(int i = 0; i < tam; i++)
    {
        if(maior<vet[i])
        {
            maior= vet[i];
        }
    }
    cout<<"O maior valor na matriz e: " <<maior;
}
void mediaValor(int vet[tamanho], int tam){
    for(i = 0; i < tam; i++){
        media = (vet[i] + media);
    }
    media = media/tam;

   cout << "A media da  matriz e: " << media << endl;
}

int main()
{
    inicializarVetor( vetor,tamanho);
    lerVetor(vetor,tamanho);
    MenorValor(vetor,tamanho);
    cout<<" "<<endl;
    maiorValor(vetor,tamanho);
    cout<<" "<<endl;
    mediaValor(vetor,tamanho);
    return 0;
}
