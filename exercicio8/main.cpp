#include <iostream>

using namespace std;
const int tamanho=20;

int vetores[tamanho];
int i;int j;

void lerVetor(int vetor[], int posicao){
    for(i=0; i < posicao; i++){
        cout << "Valor da posicao " << (i+1) << ": ";
        cin >> (vetor[i]);
    }
}
void selectionSort(int vetor[], int posicao){
    for(int i = 0; i < posicao; i++){
        int menor = i;
        for(int j =i+1; j < posicao; j++){
            if(vetor[j] < vetor[menor]){
                menor = j;
            }
        }if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
}
void imprimirVetor(int vetor[], int posicao){

    for(i=0; i < posicao; i++){
        cout << "Valores ordenados: "<< (vetor[i]) << endl;
    }
}
int main(){
    lerVetor(vetores, tamanho);
    cout<<" "<<endl;
    selectionSort(vetores,tamanho);
    imprimirVetor(vetores, tamanho);
    return 0;
}

