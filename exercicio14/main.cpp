#include <iostream>

using namespace std;

const int tamanho=10;

int vetor[tamanho];

void lerVetor(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}

void imprimirVetor(int vetor[],int tamanho){
    cout << "Valores do vetor "<<endl;
    for (int i=0; i<tamanho; i++){
        cout<<"Posicao "<<(i+1)<<": "<<vetor[i]<<endl;
    }
}

void quicksort(int vetor[],int tamanho){

    int i, j, aux;
        for( i=0; i<tamanho; i++ ){
                  for( j=i+1; j<tamanho; j++ ){
                       if( vetor[i] > vetor[j] ){
                           aux = vetor[i];
                           vetor[i] = vetor[j];
                           vetor[j] = aux;
                       }
                  }
           }
}

int main(){
    lerVetor(vetor,tamanho);
    quicksort(vetor,tamanho);
    cout<<" "<<endl;
    imprimirVetor(vetor,tamanho);
    return 0;
}
