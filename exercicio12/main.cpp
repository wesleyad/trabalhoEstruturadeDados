#include <iostream>
#include <stack>
#include <stdlib.h>

void exibirPilhaInvertida(std::stack<int>& x) {

	std::cout << "Pilha invertida:" << std::endl;

	while (!x.empty()) {
		std::cout << x.top() << " "<<std::endl;
		x.pop();
	}
}

int main() {
	std::stack<int> p;
	int numero = 0;

	std::cout << "Insira um numero para a pilha: " << std::endl;
	for (int i = 0; i <= 9; i++) {
	std::cin >> numero;
	p.push(numero);
	}
	system("cls");

	exibirPilhaInvertida(p);
    return 0;
}
