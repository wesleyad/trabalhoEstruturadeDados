#include <iostream>

using namespace std;
const int tamanho=10;

int vetores[tamanho];
int i;int j;

void lerVetor(int vetor[], int posicao){
    for(i=0; i < posicao; i++){
        cout << "Valor da posicao " << (i+1) << ": ";
        cin >> (vetor[i]);
    }
}
void bubbleSort(int vetor[],int posicao){
    for (int i=0; i < posicao; i++){
        for (int j= posicao-1; j>=i; j--){
            if (vetor[j-1]>vetor[j]){
                int temporario = vetor[j-1];
                vetor[j-1]=vetor[j];
                vetor[j]=temporario;
            }
        }
    }
}
void imprimirVetor(int vetor[], int posicao){
        for(i=0; i < posicao; i++){
        cout << "Valores ordenados: "<< (vetor[i]) << endl;
    }
}
int main(){
    lerVetor(vetores, tamanho);
    cout<<" "<<endl;
    bubbleSort(vetores,tamanho);
    imprimirVetor(vetores, tamanho);
    return 0;
}
