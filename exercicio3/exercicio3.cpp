#include <iostream>

using namespace std;

int **matriz, **matrizUm, **matrizDois;


int** inicializar(int tamanhoLinha,int tamanhoColuna){
   int** mat = new int*[tamanhoLinha];
   for (int i=0; i < tamanhoLinha; i++){
        mat[i]=new int[tamanhoColuna];//[tamanhoColuna];
   }
   return mat;
}

void lerMatriz(int *matriz[],int linha, int coluna){
    for (int i=0; i < linha; i++){
        for (int j=0; j < coluna; j++){
            cout << "Posicao["<<(i+1)<<"]["<<(j+1)<<"]: ";
            cin >> matriz[i][j];
        }
    }
}

void imprimirMatriz(int *matriz[], int linha, int coluna){
    cout << "Valores da matriz" <<endl;
    for (int i=0; i < linha; i++){
        for (int j=0; j < coluna; j++){
            cout << "Posicao["<<(i+1)<<"]["<<(j+1)<<"]: "<<matriz[i][j]<<endl;
        }
    }
}

int** multiplicarMatrizes(int *matriz1[], int *matriz2[], int linha, int coluna){
     cout << "Resultado da multiplicacao" <<endl<<endl;

     int** resultado = inicializar(linha,coluna);
     for (int i = 0; i < linha; i++){
        for (int j =0; j < coluna; j++){
            int soma = 0;
            for (int k=0; k < linha; k++){
                soma = soma + matriz1[i][k]*matriz2[k][j];
            }
            resultado[i][j]=soma;
        }
     }
     return resultado;
}
int** somaMatrizes(int *matriz1[], int *matriz2[], int linha, int coluna){
     cout << "Resultado da soma" <<endl<<endl;

     int** resultado = inicializar(linha,coluna);
     for (int i = 0; i < linha; i++){
        for (int j =0; j < coluna; j++){
            int soma = 0;
            for (int k=0; k < linha; k++){
                soma = soma + matriz1[i][k]+matriz2[k][j];
            }
            resultado[i][j]=soma;
        }
     }
     return resultado;
}

int main()
{
    cout << "Programa de multiplicar e somar matriz"<<endl;
    int linha,coluna;
    cout << "Digite a quantidade de linhas: ";
    cin >> linha;
    cout << "Digite a quantidade de colunas: ";
    cin >> coluna;

    matrizUm = inicializar(linha,coluna);
    lerMatriz(matrizUm,linha,coluna);

    matrizDois = inicializar(linha,coluna);
    lerMatriz(matrizDois,linha,coluna);

    matriz = multiplicarMatrizes(matrizUm,matrizDois,linha,coluna);
    imprimirMatriz(matriz,linha,coluna);
    cout << " " << endl;
    matriz = somaMatrizes(matrizUm,matrizDois,linha,coluna);
    imprimirMatriz(matriz,linha,coluna);
    delete matriz;
    cout << "Fim!" << endl;
    return 0;
}
