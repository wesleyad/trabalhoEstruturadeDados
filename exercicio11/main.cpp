#include <iostream>

using namespace std;
const int tamanho=10;

int vetor[tamanho];
int i;
int j;
int algoritmo;

void lerVetor(int vetor[], int posicao)
{
    for(i=0; i < posicao; i++)
    {
        cout << "Valor da posicao " << (i+1) << ": ";
        cin >> vetor[i];
    }
}
void ordenarvetor(int vetor[], int posicao)
{
    cout << "Escolha com qual algoritmo ordenar 1 = bublesort, 2 = insertionsort, 3 = selectionsort" << endl;
    cin>>algoritmo;
    switch(algoritmo){
    case 1:
        for (int i=0; i < posicao; i++)
        {
            for (int j= posicao-1; j>=i; j--)
            {
                if (vetor[j-1]>vetor[j])
                {
                    int temporario = vetor[j-1];
                    vetor[j-1]=vetor[j];
                    vetor[j]=temporario;
                }
            }

        }
        cout << "Posicao ordenada pelo bublle sort:" << endl;
        for(int i =0; i < posicao; i++)
        {
            cout << "Posicao " << (i+1) << ": " << vetor[i] << endl;
        }
    case 2:
        for (int i=1; i<posicao; i++)
        {
            int comp = vetor[i];
            int j = i - 1;
            for(; j>=0 && comp < vetor[j]; j--)
            {
                vetor[j+1]=vetor[j];
            }
            vetor[j+1] = comp;
        }
        cout << "Posicao ordenada pelo insertion sort:" << endl;
        for(int i =0; i < posicao; i++)
        {
            cout << "Posicao " << (i+1) << ": " << vetor[i] << endl;
        }
    case 3:
        for(int i = 0; i < posicao; i++)
        {
            int menor = i;
            for(int j =i+1; j < posicao; j++)
            {
                if(vetor[j] < vetor[menor])
                {
                    menor = j;
                }
            }
            if (i != menor)
            {
                int temporario = vetor[i];
                vetor[i] = vetor[menor];
                vetor[menor] = temporario;
            }
        }
        cout << "Posicao ordenada pelo selection sort:" << endl;
        for(int i =0; i < posicao; i++)
        {
            cout << "Posicao " << (i+1) << ": " << vetor[i] << endl;
        }
    }
}

int main()
{
    lerVetor(vetor, tamanho);
    cout<<" "<<endl;
    ordenarvetor(vetor,tamanho);
    return 0;
}
